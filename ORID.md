### O
- We learnt React-Router and install it on our TodoList project.
- We created new pages and use react-router for jumping between pages and rendering components.
- We created the mock API and add the path to the project.
- We used axios to implement asynchronous requests, replacing the original methods written in the reducer.
- We customized hooks, save functions that will be reused, and deconstruct calls within the component.
- When I did my homework at night, I used a lot of components from ant design, and their css was encapsulated, so it was easy to use.
### R
- Listening and coding at the same time always leaves something out.
- I learned a lot of new things today because I practiced a lot, and even though I don't understand it exactly, I can use it.
### I
- Place the <Outlet> inside the parent component, and when the <Route> generates nesting, render its corresponding subsequent child routes.
- Promise is a solution for asynchronous programming for the finalization (or failure) of an asynchronous operation and the representation of its resultant value, which makes more sense than the traditional callback function scheme.
### D
- I have to get ready for my presentation tomorrow.