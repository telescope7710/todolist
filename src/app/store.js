import {configureStore} from '@reduxjs/toolkit'
import todoReducer from '../todo/reducers/todoSlice'

export default configureStore({
    reducer: {todo: todoReducer}
})