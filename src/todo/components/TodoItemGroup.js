import TodoItem from "./TodoItem";
import {useSelector} from "react-redux";

const TodoItemGroup = () => {
    const todoList = useSelector((state) => state.todo.todoList);

    return (
        <div>
            {todoList.map((todoItem) => {
                return (
                    <TodoItem key={todoItem.id} todoItem={todoItem}/>
                );
            })}
        </div>
    )
}
export default TodoItemGroup;