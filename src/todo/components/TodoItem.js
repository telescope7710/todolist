import {useTodo} from "../../features/hooks/useTodo";
import {DeleteTwoTone, EditTwoTone, EyeTwoTone} from '@ant-design/icons';
import {useState} from "react";
import {Modal, Input, Card, Popconfirm} from "antd";
import Paragraph from "antd/lib/typography/Paragraph";
import * as todoApi from "../../apis/todo";

const TodoItem = ({todoItem}) => {
    const [InputValue, setInputValue] = useState('');
    const {updateTodo, deleteTodo} = useTodo()
    const [editVisiable, setEditVisiable] = useState(false);
    const handleToggle = async () => {
        await updateTodo(todoItem.id, {completed: !todoItem.completed})
    }
    const handleDelete = async () => {
        await deleteTodo(todoItem.id)
    }
    const handleEdit = async () => {
        await updateTodo(todoItem.id, {todoContent: InputValue})
        setEditVisiable(false)
    }
    const openEditModal = () => {
        setInputValue(todoItem.todoContent)
        setEditVisiable(true)
    }
    const closeEditModal = () => {
        setEditVisiable(false)
    }

    const changeInputValue = (event) => {
        setInputValue(event.target.value)
    }
    const openDetailModal = async () => {
        const response = await todoApi.getTodoItemById(todoItem.id)
        Modal.info({
            title: 'Todo detail',
            content: (
                <div>
                    <Paragraph>{response.data.id}</Paragraph>
                    <Paragraph>{response.data.todoContent}</Paragraph>
                    <Paragraph>{response.data.completed}</Paragraph>
                </div>
            ),
            onOk() {
            },
        })
    }

    return (<div className='todo'>
            <Card className='todoItem'>
                <div className='content'>
                    <Paragraph className={todoItem.completed ? "strike" : ""} onClick={handleToggle}>
                        {todoItem.todoContent}
                    </Paragraph></div>

                <div className='operate-bar'>
                    <EyeTwoTone onClick={openDetailModal}/>
                    <EditTwoTone onClick={openEditModal}/>
                    <Popconfirm
                        key="list-loadmore-more"
                        title="Delete the task"
                        description="Are you sure to delete this task?"
                        onConfirm={handleDelete}
                        onCancel={() => {
                        }}
                        okText="Yes"
                        cancelText="No"
                    >
                        <DeleteTwoTone twoToneColor="#eb2f96"/>
                    </Popconfirm>

                </div>
            </Card>
            <Modal title="Edit Todo content"
                   centered
                   visible={editVisiable}
                   onCancel={closeEditModal}
                   onOk={handleEdit}><Input value={InputValue} onChange={changeInputValue}/>
            </Modal>
        </div>
    )
}
export default TodoItem;