import DoneItem from "./DoneItem";
import {useSelector} from "react-redux";

const DoneGroup = () => {
    const doneTodoList = useSelector((state) => state.todo.todoList).filter(todo => todo.completed === true)
    return (
        <div className="doneGroup">
            {doneTodoList.map((todoItem) => {
                return (
                    <DoneItem key={todoItem.id} todoItem={todoItem}/>
                );
            })}
        </div>
    )
}
export default DoneGroup;