import TodoGenerator from "./TodoGenerator";
import TodoItemGroup from "./TodoItemGroup";
import {useEffect} from "react";
import {useTodo} from "../../features/hooks/useTodo";

const TodoList = () => {
    const {loadTodos} = useTodo()
    useEffect(() => {
        loadTodos()
    }, []);

    return (
        <div>
            <TodoItemGroup/>
            <TodoGenerator/>
        </div>
    )
}
export default TodoList;