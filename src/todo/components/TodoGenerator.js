import {useState} from "react";
import shortid from 'shortid'
import {useTodo} from "../../features/hooks/useTodo";
import {Input, Button} from 'antd';
const TodoGenerator = () => {
    const [InputValue, setInputValue] = useState('');
    const {createTodo} = useTodo()
    const add = async() =>{
        if (InputValue === '') {
            return
        }
        await createTodo({
            todoContent: InputValue,
            id: shortid.generate(),
            completed: false
        })
        setInputValue('');
    }
    const changeInputValue = (event) => {
        setInputValue(event.target.value)
    }
    return (
        <div className='addInput'>
            <Input className='input' type='text' value={InputValue} onChange={changeInputValue}/>
            <Button className='add' type="primary" onClick={add}>add</Button>
        </div>
    )
}
export default TodoGenerator;