import {Card, Modal, Popconfirm} from "antd";
import {DeleteTwoTone, EyeTwoTone} from "@ant-design/icons";
import Paragraph from "antd/lib/typography/Paragraph";
import {useTodo} from "../../features/hooks/useTodo";
import * as todoApi from "../../apis/todo";

const DoneItem = (props) => {
    const {deleteTodo} = useTodo()
    const {todoItem} = props;
    const handleDoneClick = async () => {
        const response = await todoApi.getTodoItemById(todoItem.id)
        Modal.info({
            title: 'Todo detail',
            content: (
                <div>
                    <Paragraph>{response.data.id}</Paragraph>
                    <Paragraph>{response.data.todoContent}</Paragraph>
                    <Paragraph>{response.data.completed}</Paragraph>
                </div>
            ),
            onOk() {
            },
        })
    }
    const handleDelete = async () => {
        await deleteTodo(todoItem.id)
    }
    return (
        <Card className='todoItem'>
            <div className='content'>
                <Paragraph className={todoItem.completed ? "strike" : ""} onClick={handleDoneClick}>
                    {todoItem.todoContent}
                </Paragraph>
            </div>


            <div className='operate-bar'>
                <EyeTwoTone onClick={handleDoneClick}/>
                <Popconfirm
                    key="list-loadmore-more"
                    title="Delete the task"
                    description="Are you sure to delete this task?"
                    onConfirm={handleDelete}
                    onCancel={() => {
                    }}
                    okText="Yes"
                    cancelText="No">
                    <DeleteTwoTone twoToneColor="#eb2f96"/>
                </Popconfirm>
            </div>
        </Card>
    )
}
export default DoneItem;