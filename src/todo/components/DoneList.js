
import DoneGroup from "./DoneGroup";
import React from "react";

const DoneList = () => {
    return (
        <div>
            <DoneGroup />
        </div>
    )
}
export default DoneList;