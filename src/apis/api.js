import axios from 'axios'
import { Spin } from 'antd'
import ReactDOM from 'react-dom'

const request = axios.create({
    timeout: 30000,
    baseURL: 'http://localhost:8083' ,
    //baseURL: 'https://64c0b6470d8e251fd11262a8.mockapi.io/api/v1'
})
let count = 0

request.interceptors.request.use(config => {
    count++
    if (count === 1) {
        let dom = document.createElement('div')
        dom.setAttribute('id', 'loading')
        document.body.appendChild(dom)
        ReactDOM.render(<Spin size="large" />, dom)
    }
    return config
})

request.interceptors.response.use(
    response => {
        count--
        if (count <= 0) {
            document.body.removeChild(document.getElementById('loading'))
        }

        if (response.status >= 200 && response.status <= 300) {
            return response
        } else {
            Promise.reject(response)
        }
    },
    error => {
        count--
        if (count <= 0) {
            document.body.removeChild(document.getElementById('loading'))
        }
        return Promise.reject(error)
    }
)

export default request
