import request from "./api";

export const getTodoItems = () => {
    return request.get("/todos")
}
export const updateTodoItems = (id, todoItem) => {
    return request.put(`/todos/${id}`, todoItem)
}
export const deleteTodoItems = (id) => {
    return request.delete(`/todos/${id}`)
}
export const createTodoItems = (todoItem) => {
    return request.post(`/todos`, todoItem)
}
export const getTodoItemById = (id) => {
    return request.get(`/todos/${id}`)
}