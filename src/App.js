import './App.css';
import {NavLink} from "react-router-dom";
import {Outlet} from "react-router";
import {Typography} from 'antd';

const {Title} = Typography;

function App() {
    return (
        <div className="App">
            <Title>Todo List</Title>
            <div className="nav-bar">
                <nav>
                    <ul>
                        <li><NavLink exact activeClassName='active' className='link' to='/'>Home</NavLink></li>
                        <li><NavLink exact activeClassName='active' className='link' to='/done'>Done List</NavLink></li>
                        <li><NavLink exact activeClassName='active' className='link' to='/help'>Help</NavLink></li>
                    </ul>
                </nav>
            </div>

            <Outlet/>
        </div>
    );
}

export default App;
