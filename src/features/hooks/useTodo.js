import {useDispatch} from "react-redux";
import * as todoApi from "../../apis/todo";
import {initTodoList} from "../../todo/reducers/todoSlice";

export const useTodo = () => {
    const dispatch = useDispatch()

    async function loadTodos() {
        const response = await todoApi.getTodoItems()
        dispatch(initTodoList(response.data))
    }


    const updateTodo = async (id, todoItem) => {
        await todoApi.updateTodoItems(id, todoItem)
        await loadTodos()
    }
    const deleteTodo = async (id) => {
        await todoApi.deleteTodoItems(id)
        await loadTodos()
    }
    const createTodo = async (todoItem) => {
        await todoApi.createTodoItems(todoItem)
        await loadTodos()
    }
    return {
        loadTodos,
        updateTodo,
        deleteTodo,
        createTodo
    }
}